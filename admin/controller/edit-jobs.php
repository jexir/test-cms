<?php

$id = get('id');
if (!$id){
    header('Location:' . admin_url('jobs'));
    exit;
}

$row = $db->from('jobs')
    ->where('job_id', $id)
    ->first();
if (!$row){
    header('Location:' . admin_url('jobs'));
    exit;
}
if (post('submit')){

    $jobs_text = post('jobs_text');
     $jobs_name = post('jobs_name');
    $jobs_status = post('jobs_status');
    $jobs_email = post('jobs_email');


    if (!filter_var($jobs_email, FILTER_VALIDATE_EMAIL)) {
    $error = "Invalid email format";
        }else {

       

            $query = $db->update('jobs')
            ->where('job_id', $id)
                ->set([
                    'job_name' => $jobs_name,
                    'job_text' => $jobs_text,
                    'job_status' => $jobs_status,
                    'job_email' => $jobs_email
                ]);

            if ($query){
                header('Location:' . admin_url('jobs'));
                 $success = 'TEST.';
            } else {
                $error = 'Have a problem.';
            }

        

    }

}

require admin_view('edit-jobs');