<?php require admin_view('static/header') ?>

    <div class="box-">
        <h1>
           Add Job
        </h1>
    </div>

    <div class="clear" style="height: 10px;"></div>

    <div class="box-" tab>

        

        <form action="" method="post" class="form label">
            <div class="tab-container">
                <div>
                    <ul>
                           <li>
                            <label>Name</label>
                            <div class="form-content">
                                <input type="text" name="jobs_name" placeholder="Enter Name">
                            </div>
                            </li>
                        <li>
                            <label>Text</label>
                            <div class="form-content">
                                <input type="text" name="jobs_text" placeholder="Enter Job Text">
                            </div>
                            </li>
                            <li>
                             <label>Email</label>
                            <div class="form-content">
                                <input type="email" name="jobs_email" placeholder="Enter email">
                            </div>
                             </li>
                            <li>
                             <label>Status</label>
                            <div class="form-content">
                                <select name="jobs_status">
                                    <option value="1">Active</option>
                                     <option value="0">Passive</option>
                                </select>
                            </div>
                        </li>
                        
                    </ul>
                </div>
                
                <ul>
                    <li class="submit">
                        <input type="hidden" name="submit" value="1">
                        <button type="submit">Save</button>
                    </li>
                </ul>
            </div>
        </form>
    </div>

<?php require admin_view('static/footer') ?>