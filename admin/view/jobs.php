<?php require admin_view('static/header') ?>

    <div class="box-">
        <h1>
            Jobs
           
                <a href="<?= admin_url('add-jobs') ?>">Add</a>
           
        </h1>
    </div>

    <div class="clear" style="height: 10px;"></div>

    <div class="table">
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>E-Mail</th>
                <th class="hide">Text</th>
                <th>Status</th>
                <th>Settings</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $row): ?>
                <tr>
                     <td>
                        <?= $row['job_name'] ?>
                    </td>
                    <td>
                        <?= $row['job_email'] ?>
                    </td>
                    <td>
                        <?= $row['job_text'] ?>
                    </td>
                     <td>
                        <?= $row['job_status']==1 ? 'Active' : "Passive" ?>
                    </td>
                    <td>
                       
                            <a href="<?= admin_url('edit-jobs?id=' . $row['job_id']) ?>" class="btn">Edit</a>
                       
                            <a onclick="return confirm('are you sure?')"
                               href="<?= admin_url('delete?table=jobs&column=job_id&id=' . $row['job_id']) ?>"
                               class="btn">Delete</a>
                      
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

<?php require admin_view('static/footer') ?>