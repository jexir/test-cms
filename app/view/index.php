<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Jobs</title>
</head>
<body>
	<hr>
	<div class="container">
  <div class="row">
<div class="col-md-6 offset-md-3">
	
	<a href="<?= site_url('add-jobs') ?>" class="float-left btn btn-success">ADD</a>
	<a href="<?=admin_url('login')?>" class="float-right btn btn-info">LOGIN</a>
	
</div>
</div>
</div>
<hr>
<?php if (isset($_GET['message'])) { ?>
  <div class="alert alert-primary" role="alert">
 <?=$_GET['message']?>
</div>
<?php } ?>
    
<div class="container">
  <div class="row">

<table class="table">
  <thead>
    <tr>
    	
     <?php $sort=='DESC' ? $sort='ASC' : $sort='DESC'; ?>
      <th width="25%" scope="col"><a href="index?<?=isset($_GET['page']) ? 'page='.$_GET['page'].'&&order=job_name&&sort='.$sort : 'order=job_name&&sort='.$sort?>">имя пользователя</a></th>
      <th width="25%" scope="col"><a href="index?<?=isset($_GET['page']) ? 'page='.$_GET['page'].'&&order=job_email&&sort='.$sort : 'order=job_email&&sort='.$sort?>">email</a></th>
      <th width="25%" scope="col"><a href="index?<?=isset($_GET['page']) ? 'page='.$_GET['page'].'&&order=job_text&&sort='.$sort : 'order=job_text&&sort='.$sort?>">текст задачи</a></th>
      <th width="25%" scope="col"><a href="index?<?=isset($_GET['page']) ? 'page='.$_GET['page'].'&&order=job_status&&sort='.$sort : 'order=job_status&&sort='.$sort?>">статус</a></th>
    </tr>
  </thead>
  <tbody>
  	 <?php foreach ($query as $row): ?>
    <tr>
      
      <td><?=$row['job_name']?></td>
      <td><?=$row['job_email']?></td>
      <td><?=$row['job_text']?></td>
      <td> <?= $row['job_status']==1 ? '<b style="color:green">выполнено </b>' : '<b style="color:red">не выполнено </b>' ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>

</table>

<nav aria-label="Page navigation example">
	<?php if ($totalRecord > $pageLimit): ?>
  <ul class="pagination">
   		
    	<?php
    		if (isset($_GET['order'])) {
    	 echo  $db->showPagination(site_url(route(1) . '?' . $pageParam . '=[page]&&order='.$_GET['order'].'&&sort='.$_GET['sort']));
    	  }

    	 else{
    	 	echo  $db->showPagination(site_url(route(1) . '?' . $pageParam . '=[page]'));
    	 }
    	 ?>
    
 
    
  </ul>
  <?php endif; ?>
</nav>
 
</div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>