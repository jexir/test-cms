<?php



$totalRecord = $db->from('jobs')
    ->select('count(job_id) as total')
    ->total();

$pageLimit = 2;
$pageParam = 'page';
$pagination = $db->pagination($totalRecord, $pageLimit, $pageParam);
if (isset($_GET['order'])) {
	$order=$_GET['order'];
}else{
	$order="job_id";
}

if (isset($_GET['sort'])) {
	$sort=$_GET['sort'];
}else{
	$sort="ASC";
	
}
$query = $db->from('jobs')
    ->orderby($order, $sort)
    ->limit($pagination['start'], $pagination['limit'])
    ->all();



require view('index');