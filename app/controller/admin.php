<?php

if (!route(1)) {
    $route[1] = 'index';
}

if (!file_exists(admin_controller(route(1)))) {
    $route[1] = 'index';
}



$menus = [

    [
        'url' => 'jobs',
        'title' => 'Jobs',
        'icon' => 'cog',
       
    ]
];

require admin_controller(route(1));